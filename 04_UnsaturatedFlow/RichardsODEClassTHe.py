#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 23 17:17:49 2017
@author: theimovaara
"""
import numpy as np
import scipy.integrate as spi
import scipy.sparse as sp

class RichardsUnsatFlow:
    def __init__(self, sPar, mDim, bPar):
        self.sPar = sPar
        self.mDim = mDim
        self.bPar = bPar
        
    def SeFun(self, hw):
        hc = -hw
        Se = (1 + ((hc * (hc > 0)) * self.sPar.vGA) ** self.sPar.vGN) ** (-self.sPar.vGM)
        return Se


    def thFun(self, hw):
        Se = self.SeFun(hw)
        th = self.sPar.thR + (self.sPar.thS - self.sPar.thR) * Se
        return th


    def CFun(self, hw):
        hc = -hw
        Se = self.SeFun(hw)
        dSedh = self.sPar.vGA * self.sPar.vGM / (1 - self.sPar.vGM) * Se ** (1 / self.sPar.vGM) * \
                (1 - Se ** (1 / self.sPar.vGM)) ** self.sPar.vGM * (hc > 0) + (hc <= 0) * 0
        return (self.sPar.thS - self.sPar.thR) * dSedh


    def CFunCmplx(self, hw):
        dh = np.sqrt(np.finfo(float).eps)
        if np.iscomplexobj(hw):
            hcmplx = hw.real + 1j*dh
        else:
            hcmplx = hw.real + 1j*dh
    
        th = self.thFun(hcmplx)
        C = th.imag / dh
        return C
    
    
    def CPrimeFun(self, hw):
        # Function for calculating the MassMatrix of the Richards Equation
        # including compression
        th = self.thFun(hw)  # volumetric water content
        Sw = th / self.sPar.thS  # water saturation
        Chw = self.CFunCmplx(hw)
        rhoW = 1000  # density of water kg/m3
        gConst = 9.82  # gravitational constant m/s2
        betaW = 4.5e-10  # compressibility of water 1/Pa
        Ssw = rhoW * gConst * (self.sPar.Cv + self.sPar.thS * betaW)
    
        cPrime = Chw + Sw * Ssw
    
        #  Ponding water condition when hw(-1) >= 0
        # if head at top is larger or equal to 0, ponding water table
        # which is implementd by changing the differential water
        # capacity in order to make the equation:
        # dhw/dt = qbnd - q(n-1/2)
        nN = self.mDim.nN
        nIN = self.mDim.nIN
        cPrime[nN-1] = 1/self.mDim.dzIN[nIN-2] * (hw[nN-1]>0) \
            + cPrime[nN-1] * (hw[nN-1]<=0)
    
        return cPrime


    def KFun(self, hw):
        nr,nc = hw.shape
        nIN = self.mDim.nIN
        Se = self.SeFun(hw)
        # kVal = sP.KSat * Se ** sP.vGE
        # * (1 - (1 - Se ** (1 / sP.vGM)) ** sP.vGM) ** 2
        kN = self.sPar.KSat * Se ** 3
    
    
        kIN = np.zeros([nIN,nc], dtype=hw.dtype)
        kIN[0] = kN[0]
        ii = np.arange(1, nIN - 1)
        kIN[ii] = np.minimum(kN[ii - 1], kN[ii])
        kIN[nIN - 1] = kN[nIN - 2]
        return kIN
    
    
    def WatFlux(self, t, hw):
        nr,nc = hw.shape
        nIN = nN = self.mDim.nIN
        dzN = self.mDim.dzN
    
        # Calculate inter nodal permeabilities
        kIN = self.KFun(hw)
    
        # Top boundary flux (Neumann flux)
        qBnd = self.bPar.topBndFuncWat(t, self.bPar)
    
        qw = np.zeros([nIN,nc], dtype=hw.dtype)
    
        if self.bPar.bottomTypeWat.lower() == 'gravity':
            qw[0] = -kIN[0]
        else:
            qw[0] = -self.bPar.kRobBotWat * (hw[0]- self.bPar.hwBotBnd)
        # Flux in all intermediate nodes
        ii = np.arange(1, nIN - 1)  # does not include last element
    
        qw[ii] = -kIN[ii] * ((hw[ii] - hw[ii - 1]) / dzN[ii - 1] + 1)
    
        # Neumann at the top
        qw[nIN - 1] = qBnd
        
    
        
        return qw
    
    
    def DivWatFlux(self, t, hw):
        nr,nc = hw.shape
        nN = self.mDim.nN
        dzIN = self.mDim.dzIN
    
        #lochw = hw.copy().reshape(mDim.zN.shape)
        divqW = np.zeros([nN,nc]).astype(hw.dtype)
        # Calculate heat fluxes accross all internodes
        cPrime = self.CPrimeFun(hw)
    
        qW = self.WatFlux(t, hw)
        # Calculate divergence of flux for all nodes
        ii = np.arange(0,nN)
        divqW[ii] = -(qW[ii + 1] - qW[ii]) / (dzIN[ii] * cPrime[ii])
    
        return divqW
    
    
    def IntegrateWF(self, tRange, iniSt):
    
        def dYdt(t, hW):
            # solver switches between zeroD and matrix shaped states
            # we need to take this into account to create a rate function that
            # works for every case...
    
            if len(hW.shape)==1:
                hW = hW.reshape(self.mDim.nN,1)
            
            rates = self.DivWatFlux(t, hW)
            return rates
    
        def jacFun(self, t, y):
            if len(y.shape)==1:
                y = y.reshape(self.mDim.nN,1)
    
            nr, nc = y.shape
            dh = np.sqrt(np.finfo(float).eps)
            jac = np.zeros((nr,nr))
            for ii in np.arange(nr):
                ycmplx = y.copy().astype(complex)
                ycmplx[ii] = ycmplx[ii] + 1j*dh
                dfdy = dYdt(t, ycmplx).imag/dh
                jac[:,ii] = dfdy.squeeze()
            #return sp.coo_matrix(jac)
            return jac
        
        # solve rate equatio
        t_span = [tRange[0],tRange[-1]]
        int_result = spi.solve_ivp(dYdt, t_span, iniSt.squeeze(),
                                   method='BDF', vectorized=True,# jac=jacFun, 
                                   t_eval=tRange,
                                   rtol=1e-6)
        return int_result



