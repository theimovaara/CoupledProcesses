#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 10 09:46:35 2018

@author: theimovaara
"""

import numpy as np
import scipy.integrate as spi
import scipy.stats as stats
from collections import namedtuple
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

class WaterBalanceLF:
    
    def __init__(self, par, tRange, lF, meteoData, stdMeas):
        
        cLcropFact = par[0]
        cLthMax = par[1]
        cLthRes = par[2]
        cLthEMax = par[3]
        cLthEMin = par[4]
        cLaPar = 10**par[5]
        cLbPar = par[6]
        cLBeta = par[7]
        
        wBthMax = par[8]
        wBthRes = par[9]
        wBaPar = 10**par[10]
        wBbPar = par[11]
        sCLIni = par[12]
        sWBIni = par[13]
        
        #derived coverlayer parameters
        cLsMax = cLthMax * lF.cLdz #max water height in cover layer
        cLsMin = cLthRes * cLsMax #minimum water height
    
        cLsEMax = cLthEMax * cLsMax # optimal water height for evaporation (below this evaporation decreases)
        cLsEMin = cLthEMin * cLsEMax #mininum water height for evaporation (evaporation becomes zero)
        
        # waste body parameters
        wBheight = lF['height'] # height of waste body
        wBsMax = wBthMax * wBheight # maximum mobile water height in waste body
        wBsMin = wBthRes * wBsMax #stagnant water height in waste body
        
        self.tRange = tRange
        self.t_span = np.array([tRange[0],tRange[-1]])
        
        self.lF = lF
    
        cL = {'dz':lF.cLdz, 
              'cFact': cLcropFact,
              'sMin': cLsMin,
              'sMax': cLsMax,
              'sEMin': cLsEMin,
              'sEMax': cLsEMax,
              'aPar': cLaPar,
              'bPar': cLbPar,
              'beta': cLBeta}
        
        self.cL = pd.Series(cL)
        
        wB = {'sMin': wBsMin,
              'sMax': wBsMax,
              'aPar': wBaPar,
              'bPar': wBbPar}
        
        self.wB = pd.Series(wB)
     
        
        self.stdMeas = stdMeas
        self.iniSt = np.array([[sCLIni * self.cL.sMax],
                      [sWBIni * self.wB.sMax]])
        
        self.meteoData = meteoData
        self.meteoTime = self.meteoData.index.astype(np.int64)/(1e9 * 3600 * 24)
    
    # Define Rate equations for coverlayer and waste body

    def leachRates(self, t, states):

        if len(states.shape) == 1:
            states = states.reshape([2,1])
    
        xy, md_ind, t_ind = np.intersect1d(self.meteoTime, np.ceil(t), return_indices=True)
    
        J = self.meteoData['rain_station'].iloc[md_ind].values.astype(states.dtype)
    
        
        sCL = states[0]
        sWB = states[1]
    
        #sC[sC < 0] = 1e-13
        #sW[sW < 0] = 1e-13
    
        sEffC = np.maximum(0, (sCL - self.cL['sMin']) /
                            (self.cL['sMax'] - self.cL['sMin'])) #%effective storage (excluding W0)
        sEffW = np.maximum(0, (sWB - self.wB['sMin']) /
                            (self.wB['sMax'] - self.wB['sMin']))
        
        #sEffC = np.minimum(1, sEffC) #%effective storage (excluding W0)
        #sEffW = np.minimum(1, sEffW)
        
        
        # sEffC = ((sCL - self.cL['sMin']) /
        #                    (self.cL['sMax'] - self.cL['sMin'])) #%effective storage (excluding W0)
        # sEffW = ((sWB - self.wB['sMin']) /
        #                    (self.wB['sMax'] - self.wB['sMin']))
        eP = self.meteoData['pEV'].iloc[md_ind].values.astype(states.dtype)
       
        eFact = ((sCL - self.cL['sEMin']) / 
            (self.cL['sEMax'] - self.cL['sEMin']) * (sCL > self.cL['sEMin']) * 
            (sCL <= self.cL['sEMax']) + (sCL > self.cL['sEMax']))
        #eFact = (temp + 30)/(avg_temp + 30)
    
        eEff =  eP * self.cL['cFact'] * eFact
    
        #Flow can only occur if effective saturation > 0
        #Effective saturation cannot exceed 1, in this case lC is equal to
        #difference between rain and effective evapotranspiration + gravity flow
        
        lC = (self.cL['aPar'] * sEffC**self.cL['bPar'] * (sEffC>0) * (sEffC<=1) + 
              (J + self.cL['aPar']) * (sEffC>1))
        
        # lC = self.cL['aPar'] * sEffC**self.cL['bPar'] * (sEffC>0)#*(sEffC<=1)
        
        lCD = self.cL['beta'] * lC
        lCW = (1 - self.cL['beta']) * lC
    
        lWD = (self.wB['aPar'] * sEffW**self.wB['bPar'] * (sEffW>0) * (sEffW<=1) + 
              (lCW + self.wB['aPar']) * (sEffW>1))
        
        # lWD = self.wB['aPar'] * sEffW**self.wB['bPar'] * (sEffW>0) #* (sEffW<=1)
         
        # Leaching rates
        rCL = J - eEff - lC
        rWB = lCW - lWD
    
        # Mass transport rates
        
        # if states.shape[1] == 1:
        #     # in ode solver
        #     rates = np.array([rCL,rWB])
        # else:
            # calculating rates and states for state matrix
        
        # check if shapes of rCL and rWB are equal
        if rCL.shape != rWB.shape:
            rCL = np.zeros(states[0].shape)
            rWB = np.zeros(states[1].shape)
        
        rates = np.stack((rCL,rWB))
        
        
        #rates = np.stack([rCL.squeeze(),rWB.squeeze()])
    
        allRates = {'J': J,
                    'eEff': eEff,
                    'lC' : lC,
                    'lCD': lCD,
                    'lCW': lCW,
                    'lWD': lWD,
                    'rCL': rCL,
                    'rWB': rWB
                    }
        
        allRates = pd.Series(allRates)
        
        allStates = {'sCL': sCL,
                     'sWB': sWB,
                     'sEffC': sEffC,
                     'sEffW': sEffW,
                     'eFact': eFact
                     }
        allStates = pd.Series(allStates)
        
        #check = J-eEff-(lCD+lWD) - (rCL+rWB)
        
        return rates, allRates, allStates

    def SimulateWatBal(self):
        
        def dYdt(t, states):
            rates, allRates, allStates = self.leachRates(t, states)
            
            return rates
        
        def jacFun(t,y):
    
            dh = np.sqrt(np.finfo(float).eps)/2
            
            nr = y.shape[0]
            # repeat approach for each state (5 times)
            jac = np.zeros([nr,nr])
        
            for ii in np.arange(nr):
                ycmplx = y.copy().astype(np.complex)
                ycmplx[ii] = ycmplx[ii] + 1j*dh
                dfdy = dYdt(t, ycmplx).imag/dh
                jac[:,ii] = dfdy.squeeze()
        
            #jac[np.abs(jac)<1e-8] = 0
            return jac
        def my_events(t,y):
            ret_val = 1
            if t in self.tRange.values:
                ret_val = 0;
            return ret_val
            
                
        # solve rate equatio
        int_result = spi.solve_ivp(dYdt, self.t_span, self.iniSt.squeeze(), 
                                   t_eval=self.tRange, 
                                   method= 'RK45', vectorized=True,# jac=jacFun,
                                   first_step = 1e-6, rtol = 1e-4
                                   )
        self.states = int_result.y
        self.rates, self.allRates, self.allStates = self.leachRates(int_result.t,self.states)
    
        # Calculate drainage rate from drainage fluxes
        self.qDr = (self.allRates['lCD'] + self.allRates['lWD'])
        self.cumqDr = np.concatenate(([0],self.qDr),axis=0).cumsum()
        return int_result


def likelihoodWatBal(par, trange, lF, meteoData, meas_data_flow, tdseries, stdMeas):
   
    # Run Forward model using parameters in par
    result = []
    
    trangeSim = trange.astype(np.int64)/(1e9 * 3600 * 24)

    watBal = WaterBalanceLF(par, trangeSim, lF, meteoData, stdMeas)
    # Call model: this function solves the problem...
    intResult = watBal.SimulateWatBal()
    
    if intResult.success: 
        
        outF_sim = watBal.cumqDr[tdseries.tcalib_ind[tdseries.tmeascal_ind]] * watBal.lF['area']
        outF_sim = outF_sim - outF_sim[0]
    
        # define objective function    
        outF = meas_data_flow['totF'].iloc[tdseries.tcalmeas_ind].values 
        like_outF_data = stats.norm(loc=np.diff(outF), scale=stdMeas)
    
        est1 = like_outF_data.logpdf(np.diff(outF_sim) )
    
        est1[np.isnan(est1)] = 0
        
        logp = np.sum(est1)
        if np.isnan(logp):
            logp = -np.inf
    else:
        logp = -np.inf

    return logp, intResult, watBal

