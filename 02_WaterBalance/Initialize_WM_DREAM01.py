"""
Created on Sat Oct 20 2018
Modules for extracting data from the Chronos database

@author: T.J. Heimovaara
"""

import numpy as np
import scipy as sp
import scipy.stats as stats
import pandas as pd
import matplotlib.pyplot as plt

import DataBaseLibrary as dbl
#from pydream.core import run_dream
from pydream.parameters import SampledParam
#from pydream.convergence import Gelman_Rubin
#import inspect

# Meteorological data will be obtained from two sources:
# 1: a close by weather station (for WM Berkhout, for BB: Lelystad)
#    we will use the evapotranspiration data obtained from the weather station...
# 2: rainfall from the 1km gridded radar corrected interpolated rainfall data obtained
#    from climate4impact...

# surface areas of Kragge compartment 3 and 4

# In[0]: Import data from KNMI
weather_station = '249'  #Berkhout
t_range = ['20120101','20190131']

pklfile = './DataFiles/meteoWM.gz'
#path = './MeteoData/WM_Rain_2008-2019.bz2'
inpfile = 'etmgeg_249.txt'
# Read data from close by meteo station
meteo_data_stat = dbl.download_meteoKNMI_etmgeg (t_range, weather_station, pklfile, inpfile)

#meteo_data_stat = dbl.download_meteoKNMI (t_range, weather_station, pklfile)
meteo_data_stat = meteo_data_stat.rename(columns={'rain':'rain_station'})

# Read data from gridded radar corrected interpolated rainfall data
#ain_radar = pd.read_pickle(fpath,compression='infer')
# transform rain values from kg/m2 (mm) to m water column
#ain_radar['rain'] = rain_radar['rain']/1e3
# Merge the station data and the interpolated rain data in to a single dataframe
meteo_data = meteo_data_stat
# meteo_data is top boundary condition. We run the model from 2003 onward
meteo_data = meteo_data[slice('2006-01-01','2019-12-31')]

pump_code = 'VP-06'
pklfile = './DataFiles/flowdata_VP-06.gz'


# In[1]: Import online sensor data TU Delft landfill database
trange = pd.date_range(start='2006-01-01',end = '2019-12-31',freq='D')
#tmeas = pd.date_range(start='2012-06-12',end = '2021-03-01',freq='D')


tcalib = pd.date_range(start='2012-07-01',end = '2019-12-31',freq='D')

df_inline = dbl.download_flow_level (pump_code, pklfile)

# We create a pivot table based on column cname (component names)
inline_par = pd.pivot_table(df_inline, values='rawvalue', index=['datetime'],
                      columns=['cname'], aggfunc=np.sum)

totF0 = pd.pivot_table(df_inline, values='value', index=['datetime'],
                      columns=['cname'], aggfunc=np.sum)

totF0['totInfilF'] = 0

totF = dbl.remove_outliers_inline(inline_par)
levelD = totF0['level']

# In[2]: Import laboratory analysis data TU Delft landfill database
## Download laboratory data for pump pit
pklfile = './DataFiles/labdata_VP-06.gz'

df_lab = dbl.download_lab(pump_code, pklfile)

# We create a pivot table based on column cname (component names)
lab_data = pd.pivot_table(df_lab, values='value', index=['date'],
                          columns=['cname'], aggfunc=np.sum)

lab_data = lab_data.rename(index=str, columns={'Ammonium (als N)': 'NH4',
                                   'Bromide': 'Br',
                                   'Chloride': 'Cl',
                                   'Totaal organisch koolstof (TOC)': 'TOC'})

lab_data.index = pd.to_datetime(lab_data.index)


# Select measurements, should fall within trange.
# tmeas contains times where measurements are available!
# can have multiple tmeas vectors for different types of measurements
# totF contains measured data from mid 2012. We choose to start on the 2012-07-01
# Because the outflow is influenced by operator decisions we choose to select 
# cumulative totals over multiple days
measFreq = 7
tmeas = pd.date_range(start='2012-07-01',end = '2019-12-31',freq='7D')
finter = sp.interpolate.interp1d(totF.index.astype(int),totF.values)
totF_val = finter(tmeas.astype(int))
totF2 = pd.DataFrame(data = totF_val, index=tmeas)
totF2 = totF2-totF2.iloc[0]
meas_data_flow = totF2.rename(columns = {0:'totF'})

#meas_data_flow['level_data'] = levelD
# Define calibration time range. This will be used by DREAM to compare
# simulated values with calibration set...
# Data set to be matched by modifying parameters...


# In order to facilitate quick and easy comparison of simulation with data
# we need to define the overlapping indices:
# tmeas_ind: trange[tmeas_ind] = tmeas
# tcalib_ind: trange[tcalib_ind]=tcalib
# tcalmeas_ind, tmeascal_ind: tmeas[tcalmeas_ind]=tcalib[tmeascal_ind]


xy, ind1, tmeas_ind = np.intersect1d(tmeas,  trange,
                                     return_indices=True)
xy, ind1, tcalib_ind = np.intersect1d(tcalib, trange,
                                      return_indices=True)
xy, tmeascal_ind, tcalmeas_ind = np.intersect1d(tcalib, tmeas,
                                                return_indices=True)

xy, tlabmeas_ind, tmeaslab_ind = np.intersect1d(lab_data.index,  trange,
                                         return_indices=True)
xy, tmeascal_lab_ind, tcalmeas_lab_ind = np.intersect1d(tcalib, lab_data.index,
                                                        return_indices=True)


tdata = {'trange':trange,
         'tmeas':tmeas,
         'tcalib':tcalib,
         'tmeas_ind':tmeas_ind,
         'tcalib_ind':tcalib_ind,
         'tcalmeas_ind':tcalmeas_ind,
         'tmeascal_ind':tmeascal_ind,
         'tlabmeas_ind': tlabmeas_ind,
         'tmeaslab_ind': tmeaslab_ind,
         'tmeascal_lab_ind': tmeascal_lab_ind,
         'tcalmeas_lab_ind': tcalmeas_lab_ind}

tdseries = pd.Series(tdata)


# Obtain landfill specific properties
cellIdx = 0 # 11N = 0, 11Z = 1, 12 = 2
lF = dbl.wastebodyPropertiesWM(cellIdx) #m2
lF['cLdz'] = 1.5 #coverlayer thickness

## Prepare DREAM model...
# Model parameters which are required to calculate fluxes etc. (often need to
# optimized).


par_d = {'cLcropFact': [0.75, 1.25, 1], #[-]
         'cLthMax': [0.15, 0.5, 0.45], #[-]
         'cLthRes': [0.0, 0.9, 0], # percentage of thTot
         'cLthEMax': [0.01, 0.9, 0.7], # percentage of thTot
         'cLthEMin': [0.01, 0.9, 0.2], # percentage of thTot
         'cLaPar': [-4, -2, np.log10(0.5e-2)], # 10 log
         'cLbpar': [0, 20, 9], #%10log! [m/d]
         'cLBeta': [0.5, 1, 0.99],
         'wBthMax': [0.15, 0.5, 0.45],
         'wBthRes': [0.0, 0.9, 0],
         'wBaPar': [-4, -2, np.log10(2e-3)], #10log
         'wBbPar': [0, 20, 7],
         'sCLIni': [0.01, 1, 0.8], #fraction of sMax
         'sWBIni': [0.01, 1, 0.8]  #fraction of sMax
         }

par_df = pd.DataFrame(data=par_d)

# lower_limits = par_df.iloc[0].values
# range_vals = par_df.iloc[1].values - par_df.iloc[0].values
# #start_pos = np.array([-3.4, 0.2, 0.8, 1.5, 0, (0.3*1+0.5*lF.height)*lF.surfArea,
# #                      0, 5, 10])

# parameters_to_sample = SampledParam(stats.uniform, loc=lower_limits, scale=range_vals)
# sampled_parameter_names = [parameters_to_sample]


stdMeas = 5
