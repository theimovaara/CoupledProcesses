"""
Created on Sat Oct 20 2018
Modules for extracting data from the Chronos database

@author: T.J. Heimovaara
"""

import numpy as np
import scipy.stats as stats
import pandas as pd
import matplotlib.pyplot as plt
from collections import namedtuple
import seaborn as sns

import DataBaseLibrary as dbl
import LFModelModules as mymod
from pydream.core import run_dream
from pydream.parameters import SampledParam
from pydream.convergence import Gelman_Rubin
import inspect


#sns.set()
#plt.close('all')

## Setup our problem
# Load data for water balance model

## Download meteo data from KNMI website
#weather_station = '269' # Lelystad
weather_station = '249' #Berkhout
t_range = ['20080101','20190101']
pklfile = 'meteoLS.bz2'
meteo_data = dbl.download_meteoKNMI (t_range, weather_station, pklfile)

## Download flow and level data from CHRONOS
pump_code = 'VP-06'
pklfile = 'flowdata_VP-06.bz2'
df_inline = dbl.download_flow_level (pump_code, pklfile)

# We create a pivot table based on column cname (component names)
inline_par = pd.pivot_table(df_inline, values='rawvalue', index=['datetime'],
                      columns=['cname'], aggfunc=np.sum)
totF = dbl.remove_outliers_inline(inline_par)

# Download laboratory data for pump pit
pklfile = 'labdata_VP-06.bz2'
pump_code = 'VP-06'

df_lab = dbl.download_lab(pump_code, pklfile)

# We create a pivot table based on column cname (component names)
lab_data = pd.pivot_table(df_lab, values='value', index=['date'],
                      columns=['cname'], aggfunc=np.sum)

lab_data = lab_data.rename(index=str, columns={'Ammonium (als N)': 'NH4',
                                      'Bromide': 'Br',
                                      'Chloride': 'Cl',
                                      'Totaal organisch koolstof (TOC)': 'TOC'})
lab_data.index = pd.to_datetime(lab_data.index)


trange = pd.date_range(start='2008-01-01',end = '2018-09-12',freq='D')
#trange = np.arange(dt.date(2008,1,1).toordinal(),dt.date(2018,9,12).toordinal()+1)




meteo_data = meteo_data[slice('2008-01-01','2018-12-31')]
totF2 = totF[slice('2008-01-01','2018-12-31')].resample('D').interpolate(method='linear')

# Export data to spreadsheets...
meteo_data.to_excel('20190430_WieringermeerData_Meteo.xlsx',sheet_name='meteo_data')
totF2.to_excel('20190430_WieringermeerData_LeachateProduction.xlsx',sheet_name='outflow_data')


# Estimate inital landfill surface area based on rough mass balance
#cropF = 1;
#netRain = meteo_data['rain'].sum()-cropF*meteo_data['pEV'].sum()

# Estimate landfill surface area
#totQ = totF['totF'].iloc[-1] - totF['totF'].iloc[0]
#aLF = totQ/netRain
#Get the length of the simulation time range. This will be the minimum number
# ranked storage ages we will consider. At the same time it is the time range...

# Obtain landfill specific properties
