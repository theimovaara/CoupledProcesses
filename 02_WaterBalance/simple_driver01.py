#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 30 14:01:54 2021

@author: theimovaara
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import LFModelModules as mymod

sns.set()

# In[1]: Initialize

par_d = {'cLcropFact': 1, #[-]
         'cLthMax':  0.45, #[-]
         'cLthRes': 0., # percentage of thTot
         'cLthEMax': 0.7, # percentage of thTot
         'cLthEMin': 0.2, # percentage of thTot
         'cLaPar': np.log10(0.5e-2), # 10 log
         'cLbpar': 9,#%10log! [m/d]
         'cLBeta': 0.99,
         'wBthMax': 0.5,
         'wBthRes': 0.0,
         'wBaPar': np.log10(2e-3), #10log
         'wBbPar': 7,
         'sCLIni': 0.8,
         'sWBIni': 0.8
         }

par_d = pd.Series(par_d)

lF = {'cLdz': 1.5, #%m
      'area': 28355,
      'height': 12}

lF = pd.Series(lF)


# Read meteodata

meteo_data  = pd.read_excel('WieringermeerData_Meteo.xlsx')
meteo_data['num_date'] = meteo_data['datetime'].astype(np.int64)/(1e9*3600*24)
meteo_data.set_index('datetime',inplace=True)
t_range = meteo_data['num_date'][:-1]
taxis = meteo_data.index[:-1]

# Read measurement data
meas_data = pd.read_excel('WieringermeerData_LeachateProduction.xlsx')
colnames = meas_data.columns
meas_data = meas_data.rename(columns={colnames[0]:'datetime',
                          colnames[1]:'cumOutflow'})
meas_data['num_date'] = meas_data['datetime'].astype(np.int64)/(1e9*3600*24)

stdMeas = 10


# Inicialize class
myWatBal = mymod.WaterBalanceLF (par_d, t_range, lF, meteo_data, stdMeas)

# In[2]: Dynamic
int_result = myWatBal.SimulateWatBal()

simRates = myWatBal.leachRates(int_result.t, int_result.y)


# In[3]: Ooutput
if int_result.success:
    print('integration successful!')

    
    # plot states
    plt.close('all')
    
    fig1,ax1 = plt.subplots(1,1,sharex=True)
    # plot qinf
    ax1.plot(taxis,int_result.y[0,:])
    ax1.plot(taxis,int_result.y[1,:])
    ax1.set_ylabel('$S$')
    ax1.set_xlabel('time')
    
    fig2,ax2 = plt.subplots(1,1,sharex=True)
    # plot qinf
    ax2.plot(taxis,myWatBal.allStates['sEffC'])
    ax2.plot(taxis,myWatBal.allStates['sEffW'])
    ax2.set_ylabel('$S_{eff}$')
    ax2.set_xlabel('time')
    
    #plot measured data versus simulated data 
    # find overlapping time inidices
    
    val, sim_idx, meas_idx = np.intersect1d(t_range,meas_data.num_date,return_indices = True)
    
    fig3 = plt.figure(3)
    fig3.clf()
    plt.plot(taxis[sim_idx[:]],meas_data['cumOutflow'].iloc[meas_idx[:]]/lF['area'])
    plt.plot(taxis[sim_idx[:]],(myWatBal.cumqDr[sim_idx[:]]-myWatBal.cumqDr[sim_idx[0]]))
    plt.xlabel('time')
    plt.ylabel('cumDrainage [m]')
    plt.legend(['measured', 'simulated'])
    
    fig4 = plt.figure(4)
    fig4.clf()
    plt.plot(taxis[sim_idx[1:]],meas_data['cumOutflow'].iloc[meas_idx[1:]].diff() / lF['area'])
    plt.plot(taxis[sim_idx[:]],myWatBal.qDr[sim_idx[:]])
    plt.xlabel('time')
    plt.ylabel('Drainage rate [m/d]')
    plt.legend(['measured', 'simulated'])
    
    #Plot mass balance in coverlayer
    
    mbalCL = np.diff(myWatBal.allStates['sCL'])/np.diff(t_range) - \
               (myWatBal.allRates['J']-myWatBal.allRates['eEff'] - \
                myWatBal.allRates['lC'])[:-1]
    
    mbalWB = np.diff(myWatBal.allStates['sWB'])/np.diff(t_range) - \
               (myWatBal.allRates['lCW'] - myWatBal.allRates['lWD'])[:-1]
    
    
    
    fig5,ax5 = plt.subplots(2,1,sharex=True)
    ax5[0].plot(taxis[:],np.concatenate([[0],mbalCL],axis=0).cumsum())
    ax5[1].plot(taxis[:],np.concatenate([[0],mbalWB],axis=0).cumsum())
    ax5[0].set_ylabel('mass balance CL')
    ax5[1].set_ylabel('mass balance WB')
    ax5[1].set_xlabel('time')
    
    fig6,ax6 = plt.subplots(2,1,sharex=True)
    # plot qinf
    ax6[0].plot(taxis,myWatBal.allRates['lC'])
    ax6[1].plot(taxis,myWatBal.allRates['lWD'])
    ax6[0].set_ylabel('lC')
    ax6[1].set_ylabel('lW')
    ax6[1].set_xlabel('time')
    
    
    
    #overall mass balance
    del_sCL = myWatBal.allStates['sCL'][-1] - myWatBal.allStates['sCL'][0]
    del_sWB = myWatBal.allStates['sWB'][-1] - myWatBal.allStates['sWB'][0]
    
    delFCL = np.sum(myWatBal.allRates['J']-myWatBal.allRates['eEff']-myWatBal.allRates['lC'])

else:
    print('integration failed')