"""
Created on Sat Oct 20 2018
Modules for extracting data from the Chronos database

@author: T.J. Heimovaara
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import scipy.stats as scpstats
import pickle as pkl

from pydream.convergence import Gelman_Rubin

import LFModelModules as mymod
import Initialize_WM_DREAM01 as iWM

# set seaborn graphics style
sns.set()

def likelihood(par):
    # Run Forward model using parameters in par
    logp, intResult, watBal \
        = mymod.likelihoodWatBal(par, iWM.trange, iWM.lF, 
                                 iWM.meteo_data, iWM.meas_data_flow, 
                                 iWM.tdseries, iWM.stdMeas)
    return logp, intResult, watBal


d_iter = 100
nchains = 5
d_max = 19800
npar = iWM.par_df.shape[1]
parfn = './Results/WM01'
logpsfn = './Results/WM01_logps'

sampled_params = np.zeros([nchains,d_max,npar])
log_ps = np.zeros([nchains,d_max])

# Loop through all files
for chain in np.arange(nchains):
    for nsamp in np.arange(d_iter,d_max+d_iter,d_iter):
        fn = parfn + str(chain) + '_' + str(nsamp) + '.npy'
        fnlogps = logpsfn + str(chain) + '_' + str(nsamp) + '.npy'
        sampled_params[chain][nsamp-d_iter:nsamp] = np.load(fn, allow_pickle=False)
        log_ps[chain][nsamp-d_iter:nsamp] = np.load(fnlogps).squeeze()
        starts = [sampled_params[chain][-1, :] for chain in range(nchains)]

maxidx = np.unravel_index(log_ps.argmax(), log_ps.shape)
par = sampled_params[maxidx]
GR = Gelman_Rubin(sampled_params)


# save starts for possible restart of dream optimization
#save starts to pickle...
fo = open('starts01.pkl','wb')
pkl.dump(starts,fo)
fo.close()


# fo = open('starts01.pkl','rb')
# starts = pkl.load(fo)
# par = starts[0]

# par = iWM.par_df.iloc[2].values
# starts = [par]

logp_tot = []
cumqDr_tot = []
watBal_tot = []
intResult_tot = []
totF_sim = []

par_set = starts
par_setdf = pd.DataFrame(par_set,columns=iWM.par_df.columns)

for ii in np.arange(len(par_set)):
    # Run model for parameters in chain
    logp, intResult, watBal \
        = likelihood(par_set[ii])

    logp_tot.append(logp)
    watBal_tot.append(watBal)
    intResult_tot.append(intResult)
    
    totF_sim.append(watBal.cumqDr[iWM.tmeas_ind] - watBal.cumqDr[iWM.tmeas_ind[0]])
    
#import make_plotsBB
#cLOut = [qinf, qrf, qev, seff, th, mbalerr, cInf, m]

best_par = pd.Series(par,index=iWM.par_df.columns)
allpardf = par_setdf.append(best_par,ignore_index=True)
print(allpardf.T)

loc_name = 'Wieringermeer'

plt.close('all')

fig0 = plt.figure(0)
plt.plot(log_ps.T,'.')
plt.title('log_ps')


fig1 = plt.figure(1)
plt.clf()
plt.plot(iWM.meas_data_flow.index,iWM.meas_data_flow['totF'].values/iWM.lF.area,'o')
for ii in range(len(par_set)):
    plt.plot(iWM.tmeas, totF_sim[ii])
plt.xlabel('year')
plt.ylabel(r'cumulative leachate [$m$]')
plt.title(loc_name)
plt.legend(['measured'])


fig2 = plt.figure(2)
plt.clf()
plt.plot(iWM.tmeas,iWM.meas_data_flow.diff()/iWM.measFreq/iWM.lF.area, 'g.')
for ii in range(len(par_set)):
    plt.plot(iWM.tmeas[1:], np.diff(totF_sim[ii])/iWM.measFreq)
plt.xlabel('year')
plt.ylabel(r'leachate pump rate [$m^3/day]$')
plt.title(loc_name)


fig3 = plt.figure(3)
plt.clf()
for ii in range(len(par_set)):
    totF = (watBal_tot[ii].cumqDr - watBal_tot[ii].cumqDr[iWM.tmeas_ind[0]])
    plt.plot(iWM.trange,np.diff(totF))
             
plt.title('long term, water flow')
plt.xlabel('year')
plt.ylabel('leachate pumprate [$m^3/d$]')

fig4,ax4 = plt.subplots(2,1,sharex=True)
# plot qinf
for ii in range(len(par_set)):
    sEffCL = np.maximum(0,(intResult_tot[ii].y[0,:]-watBal_tot[ii].cL['sMin']) /
                        (watBal_tot[ii].cL['sMax']-watBal_tot[ii].cL['sMin']))
    sEffWB = np.maximum(0,(intResult_tot[ii].y[1,:]-watBal_tot[ii].wB['sMin']) /
                        (watBal_tot[ii].wB['sMax']-watBal_tot[ii].wB['sMin']))
    ax4[0].plot(iWM.trange,sEffCL)
    ax4[1].plot(iWM.trange,sEffWB)
    
plt.title('States')
ax4[1].set_xlabel('year')
ax4[0].set_ylabel(r'$S_{eff_{CL}}$')
ax4[1].set_ylabel(r'$S_{eff_{WB}}$')

fig5,ax5 = plt.subplots(2,1,sharex=True)
# plot qinf
[ax5[0].plot(iWM.trange,watBal_tot[ii].allRates['lC']) for ii in range(len(par_set))]
[ax5[1].plot(iWM.trange,watBal_tot[ii].allRates['lWD']) for ii in range(len(par_set))]
plt.title('leaching rates')
ax5[1].set_xlabel('year')
ax5[0].set_ylabel(r'$L_{C}$')
ax5[1].set_ylabel(r'$L_{WD}$')


plot_chains = False
if plot_chains:
    plt.close('all')
    for ii in np.arange(npar):
        plt.figure()
        plt.clf()
        plt.plot(sampled_params[:,:,ii].T,'.')
        plt.title(iWM.par_df.columns[ii])

test = []    
for ii in np.arange(5):
    test.append(pd.DataFrame(sampled_params[ii,:,:],columns=iWM.par_df.columns))
    
pd.plotting.scatter_matrix(test[0].iloc[-500:],alpha=0.2)
