# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import LFModelModules as mymod

sns.set()

par_d = {'cLcropFact': 1, #[-]
         'cLthMax':  0.45, #[-]
         'cLthRes': 0., # percentage of thTot
         'cLthEMax': 0.7, # percentage of thTot
         'cLthEMin': 0.2, # percentage of thTot
         'cLaPar': 0.5e-2, # 10 log
         'cLbpar': 9,#%10log! [m/d]
         'cLBeta': 0.99,
         'wBthMax': 0.5,
         'wBthRes': 0.0,
         'wBaPar': 2e-3, #10log
         'wBbPar': 7,
         'sCLIni': 0.8,
         'sWBIni': 0.8
         }

par_d = pd.Series(par_d)

lF = {'cLdz': 1.5, #%m
      'area': 28355,
      'height': 12}

lF = pd.Series(lF)


# Read meteodata

meteo_data  = pd.read_excel('WieringermeerData_Meteo.xlsx')
meteo_data['num_date'] = meteo_data['datetime'].astype(np.int64)/(1e9*3600*24)
meteo_data.set_index('datetime',inplace=True)
t_range = meteo_data['num_date'][:-1]
taxis = meteo_data.index[:-1]

# In[3] Read measurement data
meas_data = pd.read_excel('WieringermeerData_LeachateProduction.xlsx')
colnames = meas_data.columns
meas_data = meas_data.rename(columns={colnames[0]:'datetime',
                          colnames[1]:'cumOutflow'})
meas_data['num_date'] = meas_data['datetime'].astype(np.int64)/(1e9*3600*24)

stdMeas = 1

myWatBal = mymod.WaterBalanceLF (par_d, t_range, lF, meteo_data, stdMeas)


# In[4]: test
sTst = np.arange(0,myWatBal.cL.sMax,0.01)
sTstW = np.ones(sTst.shape)
tst = np.stack([sTst,sTstW])
ttst = np.ones(sTst.shape)*t_range[19]
aap = myWatBal.leachRates(ttst,tst)
rates1, allRates1, allStates1 = aap
