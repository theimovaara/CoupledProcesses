#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 28 08:15:49 2017

@author: theimovaara
"""

# matplotlib inline
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as spi
import MyTicToc as mt

# Definition of parameters
class LotkaVolterra:
    """ 
    Class for solving a Lotka-Volterra predator pray problem as an example
    code which forms the base for extension to other types of problems
    """
    
    # Initialize classs from input parameters...
    def __init__(self, tRange, iniSt, LV_par):
        
        self.tRange = tRange
        self.t_span = np.array([tRange[0],tRange[-1]])
        self.prey_birth = LV_par['prey_birth']
        self.prey_death = LV_par['prey_death']
        self.pred_birth = LV_par['pred_birth']
        self.pred_death = LV_par['pred_death']
        self.initSt = iniSt
        

    # Definition of Rate Equation
    def LV_dYdt(self, t, states):
        """ Return the growth rate of fox and rabbit populations. """
        
        rates_prey = self.prey_birth * states[0] \
            - self.prey_death * states[0] * states[1]
        rates_pred = self.pred_birth * states[0] * states[1] \
            - self.pred_death * states[1]
         
        #return as column vector (for vectorised problems)   
        return np.array([[rates_prey],
                        [rates_pred]])
    
    def IntegrateLV(self):
        
        def dYdt(t, states):
            rates = self.LV_dYdt(t, states)
            return rates
        # solve rate equatio
        int_result = spi.solve_ivp(dYdt, self.t_span, iniSt.squeeze(), 
                                    t_eval=self.tRange, 
                                    method='RK45', vectorized=True, 
                                    rtol=1e-5 )
        self.states = int_result.y
        return int_result
        
        

#def main():
    # Definition of output times


# Initialize problem

# time range
tRange = np.arange(0,500,0.250)              # time
nOut = np.shape(tRange)[0]

# Initial states, 10 rabbits, 5 foxes
num_prey = 5
num_pred = 5

# model parameters
LV_par = {'prey_birth': 1,
          'prey_death': 0.125,
          'pred_birth': 0.01,
          'pred_death': 0.08}

    # states should be a column vector for running built in solver in 
    # vectorized mode...
iniSt = np.array([[num_prey],[num_pred]])
    
MyLV = LotkaVolterra(tRange, iniSt, LV_par)                  


# Dynamic Part of the simulation (Solve the states across time range)                      
mt.tic()
int_res = MyLV.IntegrateLV()
mt.toc()

# Process results, generate output...
# Plot results with matplotlib
plt.close('all')

plt.figure(1)
plt.plot(tRange, MyLV.states[0,:], 'r-', label='prey')
plt.plot(tRange, MyLV.states[1,:], 'g-', label='pred')
    
plt.grid()
plt.legend(loc='best')
plt.xlabel('time')
plt.ylabel('population')
plt.title('Evolution of predator and prey populations')
plt.show()

plt.figure(2)
plt.plot(MyLV.states[0,:], MyLV.states[1,:], 'b-')
    
plt.grid()

plt.xlabel('prey')    
plt.ylabel('pred')
plt.title('Evolution of predator and prey populations')
# f2.savefig('rabbits_and_foxes_2.png')
plt.show()


# if __name__ == "__main__":
#     main()
